import my_fsm

hits = {}
misses = {}
notengfalsealarms = {}
notengcorrectrejections = {}
baselinefalsealarms = {}
baselinecorrectrejections = {}

englength = len(open("English.txt").readlines())
notenglength = len(open("NotEnglish.txt").readlines())
baselinelength = len(open("Baseline.txt").readlines())

for i in range(2, 6):
    thefsm = my_fsm.fsm(i, "Tweets.txt")
    engreject = thefsm.reject("English.txt")
    notengreject = thefsm.reject("NotEnglish.txt")
    baselinereject = thefsm.reject("Baseline.txt")
    
    hits[i] = float(englength - engreject) # English-accepted
    misses[i] = float(engreject)
    notengfalsealarms[i] = float(notenglength - notengreject) # NotEnglish-accepted
    notengcorrectrejections[i] = float(notengreject)
    baselinefalsealarms[i] = float(baselinelength - baselinereject) # Baseline-accepted
    baselinecorrectrejections[i] = float(baselinereject)
    
    print "Order: " + str(i)
    print "---"
    print "Hits: " + str(hits[i])
    print "Misses: " + str(misses[i])
    print "---"
    print "B falsealarms: " + str(baselinefalsealarms[i])
    print "B correctrejections: " + str(baselinecorrectrejections[i])
    print "---"
    print "NE falsealarms: " + str(notengfalsealarms[i])
    print "NE correctrej: " + str(notengcorrectrejections[i])
    print "---"
    print "recall: " + str(hits[i]/(hits[i] + misses[i]))
    print "B corecall: " + str(baselinefalsealarms[i]/(baselinefalsealarms[i] + baselinecorrectrejections[i]))
    print "NE corecall: " + str(notengfalsealarms[i]/(notengfalsealarms[i] + notengcorrectrejections[i]))
    print "B precision: " + str(hits[i]/(hits[i] + baselinefalsealarms[i]))
    print "B coprecision: " + str(misses[i]/(misses[i] + baselinecorrectrejections[i]))
    print "NE precision: " + str(hits[i]/(hits[i] + notengfalsealarms[i]))
    print "NE coprecision: " + str(misses[i]/(misses[i] + notengcorrectrejections[i]))
    print "B accuracy: " + str((hits[i] + baselinecorrectrejections[i])/(hits[i] + misses[i] + baselinefalsealarms[i] + baselinecorrectrejections[i]))
    print "NE accuracy: " + str((hits[i] + notengcorrectrejections[i])/(hits[i] + misses[i] + notengfalsealarms[i] + notengcorrectrejections[i]))
    print "------\n"



# - hit: The string is legal, and the parser accepted it. (True-True).
#     English-accepted
# - miss: The string is legal, but the parser rejected it. (True-False).
#     English-rejected
# - false alarm: The string is illegal, but the parser accepted it. (False- True).
#     NotEnglish-accepted, Baseline-accepted
# - correct rejection: The string is illegal, and the parser rejected it. (False- False)
#     NotEnglish-rejected, Baseline-rejected