import random

# Read in the original tweets
theFile = open("Tweets.txt")
tweetlines = theFile.readlines()
theFile.close()

# Go through the tweets and do two things: get the length of each tweet, and add each tweet to a massive string for later use.
fullstring = ""
linelens = []
for line in tweetlines:
    line = line.replace('\n','')
    fullstring += line
    linelens.append(len(line))

# Randomize the lengths of the tweets. I'll still use the lengths from the original, but in a different order.
random.shuffle(linelens)

# The overall idea of my approach is to find the distribution of letters in the seed file, then create a look-up table for the cumulative distribution. To generate a letter for a new tweet, generate a random number between 0 and 1, then go through the look-up table to find which letter it's below.

alph = " abcdefghijklmnopqrstuvwxyz" # the alphabet, including a space.
dist = {} # to hold the letter distributions
stackeddist = {} # to hold the cumulative distributions
fulllength = float(len(fullstring)) # the length of the entire set of tweets, for normalizing probabilities

# Go through the alphabet counting how many of each letter occurs in the text. Add the appropriate values to their respective dicts.
for i, letter in enumerate(alph):
    dist[letter] = float(fullstring.count(letter))/fulllength
    if i == 0:
        stackeddist[letter] = dist[letter]
    else:
        stackeddist[letter] = stackeddist[alph[i-1]] + dist[letter]

newtweets = [] # to hold the generated tweets
newtweetsstring = "" # to hold the full string of new tweets. used for checking the distribution

# Go through the list of tweet lengths. Using the cumulative distribution look-up table, figure out which letter to output based on a random number between 0 and 1.
for length in linelens:
    newtweet = ''
    for i in range(length):
        x = random.random()
        for letter in alph:
            if x < stackeddist[letter]:
                newtweet += letter
                break
    newtweets.append(newtweet)
    newtweetsstring += newtweet

# To verify the distribution of the new file, do the same thing I did to Tweets to the new one.
baselinedist = {}
newfulllength = len(newtweetsstring)
for letter in alph:
    baselinedist[letter] = float(newtweetsstring.count(letter))/newfulllength

# Print out the distributions from the two files and the differences between them.
compareison = {}
for letter in alph:
    compareison[letter] = (dist[letter], baselinedist[letter], dist[letter] - baselinedist[letter])
    print letter + ': ' + str(compareison[letter]).strip("()")

# Output the set of new tweets to a text file. 
outfile = open("Baseline.txt", "w")
for line in newtweets:
    outfile.write(line + '\n')
outfile.close()